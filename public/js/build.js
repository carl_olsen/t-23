(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
'use strict';

var spriteTypes = require('./sprite');


var LevelDataParser = {
    game: null,

    parse: function(levelData) {
        var self = this;
        levelData.data.layers.forEach(function(layer) {
            if (layer.type === 'imagelayer') {
                self.loadImageLayer(layer);
            }

            if (layer.type === 'objectgroup') {
                self.loadObjectGroupLayer(layer);
            }
        });
    },

    loadImageLayer: function(layer) {
        var sprite = this.game.add.sprite(layer.x, layer.y, layer.name);
        this.game.groups.bg.add(sprite);
    },

    loadObjectGroupLayer: function(layer) {
        var game = this.game;

        layer.objects.forEach(function(obj) {
            var properties = obj.properties || {};
            var type       = properties.type;
            var SpriteType = spriteTypes[type];
            if (!SpriteType) {
                return;
            }

            var sprite = new SpriteType(game, obj.x, obj.y);
            sprite.name   = obj.name;
            sprite.width  = obj.width;
            sprite.height = obj.height;

            if (sprite.initProperties) {
                sprite.initProperties(properties);
            }

            game.groups.level.add(sprite);
        });
    }
};


var factory = function(game) {
    var obj = Object.create(LevelDataParser);
    obj.game = game;
    return obj;
};
module.exports = factory;
},{"./sprite":7}],2:[function(require,module,exports){
'use strict';

var Bullet    = require('./sprite/bullet');
var ClickArea = require('./sprite/click-area');
var Player    = require('./sprite/player');

var Level = function(obj) {

    var base = {
        initCore: function() {

            var game = this.game;

            this.physics.arcade.gravity.y = 1000;

            if (!game.player) {
                this.initGameObjects();
            }

            this.player  = game.player;
            this.sprites = game.sprites;
            this.groups  = game.groups;
        },
        initGameObjects: function() {
            var game = this.game;

            var bg            = this.add.group(this.world, 'bg');
            var fireClickArea = this.add.existing(new ClickArea(game))
            var level         = this.add.group(this.world, 'level');
            var enemies       = this.add.group(this.world, 'enemies');
            var player        = this.add.existing(new Player(game))

            fireClickArea.events.onInputDown.add(function(pointer) {
                player.fire();
            });

            game.player = player;

            game.sprites = {
                fireClickArea: fireClickArea,
            };

            game.groups = {
                bg:      bg,
                level:   level,
                bullets: player.weapon.bullets,
                enemies: enemies,
            };
        },

        initLevelData: function() {},

        resetCore: function() {
            this.groups.enemies.removeAll();
        },

    }

    return Object.assign(base, obj);

};

module.exports = Level;
},{"./sprite/bullet":4,"./sprite/click-area":5,"./sprite/player":8}],3:[function(require,module,exports){
'use strict';

var extend = function(parentConsturctor, subProto) {
    var c = subProto.constructor;
    c.prototype.constructor = c;
    c.prototype             = Object.assign(
        Object.create(parentConsturctor.prototype),
        subProto
    );
    return c;
};

Phaser.extend = extend;

var targets = [
    Phaser.Sprite,
    Phaser.Group
];

targets.forEach(function(target){
    target.extend = function(proto){
        return extend(target, proto);
    };
});

},{}],4:[function(require,module,exports){
'use strict';

var Bullet = Phaser.Sprite.extend({
    constructor: function Sprite(game, x, y, texture) {
        texture = texture || game.cache.getBitmapData('bullet');

        Phaser.Sprite.call(this, game, x, y, texture);

        game.physics.enable(this, Phaser.Physics.ARCADE);

        this.anchor.set(0.5);
        this.checkWorldBounds = true;
        this.outOfBoundsKill  = true;

        this.body.allowGravity = false;
    }
});

module.exports = Bullet;
},{}],5:[function(require,module,exports){
'use strict';

var ClickArea = Phaser.Sprite.extend({
    constructor: function Sprite(game, x, y, texture) {
        texture = texture || game.cache.getBitmapData('box');

        Phaser.Sprite.call(this, game, x, y, texture);

        this.alpha        = 0;
        this.width        = game.width;
        this.height       = game.height;
        this.inputEnabled = true;
   }
});

module.exports = ClickArea
},{}],6:[function(require,module,exports){
'use strict';

var Enemy = Phaser.Sprite.extend({
    constructor: function Sprite(game, x, y, texture) {
        texture = texture || game.cache.getBitmapData('box');

        Phaser.Sprite.call(this, game, x, y, texture);

        game.physics.enable(this, Phaser.Physics.ARCADE);

        this.anchor.set(0.5);
        this.width  = 20;
        this.height = 20;
    },
    update: function() {
        // this.body.velocity.x = 0;
    }
});

module.exports = Enemy;
},{}],7:[function(require,module,exports){
'use strict';

module.exports = {
    Static: require('./static'),
    Enemy: require('./enemy'),
};
},{"./enemy":6,"./static":9}],8:[function(require,module,exports){
'use strict';

var Player = Phaser.Sprite.extend({
    constructor: function Sprite(game, x, y, texture) {
        texture = texture || 'player';
        Phaser.Sprite.call(this, game, x, y, texture);

        game.physics.enable(this, Phaser.Physics.ARCADE);
        this.body.collideWorldBounds = true;
        this.body.setSize(
            14,
            40,
            0,
            0
        );
        this.anchor.set(0.5);
        this.lastJump           = 0;
        this.body.maxVelocity.y = 500;

        this.animations.add('idle', [1], 15, true);

        var legs = this.legs = game.add.sprite(0, 0, 'player');
        legs.anchor.set(0.5);
        legs.animations.add('idle', [0], 15, true);
        legs.animations.add('walk', [2, 3, 4, 5, 6, 7, 8, 9, 10, 11], 15, true);
        legs.animations.add('walk_reverse', [2, 3, 4, 5, 6, 7, 8, 9, 10, 11].reverse(), 15, true);

        var torso = this.torso = game.add.sprite(0, 0, 'player');
        torso.defaultY  = torso.y;
        torso.stepDownY = torso.y + 1;
        torso.anchor.set(0.5);
        torso.animations.add('idle', [12], 15, true);
        torso.animations.play('idle');

        var armX = -1;
        var armY = -10;

        var arm1 = this.arm1 = game.add.sprite(armX, armY, 'player-arm');
        arm1.defaultY  = arm1.y;
        arm1.stepDownY = arm1.y + 1;
        arm1.anchor.set(0.1, 0.5);
        arm1.animations.add('idle', [0], 15, true);
        arm1.animations.add('fire', [1, 2, 3, 2, 1, 0], 50);

        var arm2 = this.arm2 = game.add.sprite(armX, armY, 'player-arm');
        arm2.defaultY  = arm2.y;
        arm2.stepDownY = arm2.y + 1;
        arm2.anchor.set(0.1, 0.5);
        arm2.animations.add('idle', [8], 15, true);


        this.addChild(legs);

        this.addChild(arm2);
        this.addChild(torso);
        this.addChild(arm1);

        this.animations.play('idle');
        legs.animations.play('idle');
        arm1.animations.play('idle');
        arm2.animations.play('idle');


        var weapon = game.add.weapon(20, game.cache.getBitmapData('bullet'));

        weapon.bulletKillType = Phaser.Weapon.KILL_WORLD_BOUNDS;
        weapon.bulletSpeed    = 800;
        weapon.fireRate       = 60;
        weapon.bullets.setAll('body.allowGravity', false);

        this.weapon = weapon;

    },
    update: function() {
        var game      = this.game;
        var keyAction = game.input.action;
        var LEFT      = keyAction.left.isDown || keyAction.left_alt.isDown;
        var RIGHT     = keyAction.right.isDown || keyAction.right_alt.isDown;
        var JUMP      = keyAction.jump.isDown || keyAction.jump_alt.isDown || keyAction.up.isDown || keyAction.up_alt.isDown;

        var walkVel      = 100;
        var jumpVel      = 300;
        var jumpCooldown = 750;

        this.body.velocity.x = 0;

        var mx          = game.input.activePointer.worldX;
        var mRightOfArm = this.mouseRightOfArm = mx > this.arm1.world.x;

        if (
            JUMP &&
            (this.body.touching.down || this.body.onFloor()) &&
            game.time.now > this.lastJump
        ) {
            this.body.velocity.y = -jumpVel;
            this.lastJump        = game.time.now + jumpCooldown;
        }

        var legsAnim;

        this.legs.scale.x = mRightOfArm ? 1 : -1;
        if (LEFT) {
            this.body.velocity.x = -walkVel;
            legsAnim             = mRightOfArm ? 'walk_reverse' : 'walk';

        } else if (RIGHT) {
            this.body.velocity.x = walkVel;
            legsAnim             = mRightOfArm ? 'walk' : 'walk_reverse';

        } else {
            legsAnim = 'idle';
        }

        this.legs.animations.play(legsAnim);

        this.arm1.rotation = this.arm2.rotation = game.physics.arcade.angleToPointer(this.arm1.world);

        var currentLegFrame = this.legs.animations.currentFrame.index;
        var stepDownFrames  = [1, 2, 6, 7];

        if (stepDownFrames.indexOf(currentLegFrame) !== -1) {
            this.arm1.y  = this.arm1.stepDownY;
            this.arm2.y  = this.arm2.stepDownY;
            this.torso.y = 1;
        } else {
            this.arm1.y  = this.arm1.defaultY;
            this.arm2.y  = this.arm2.defaultY;
            this.torso.y = 0;
        }

        this.torso.scale.x = mRightOfArm ? 1 : -1;
        this.arm1.scale.y  = this.arm2.scale.y = mRightOfArm ? 1 : -1;
    },
    getGunTip: function() {
        var offsetX  = 24;
        var offsetY  = -2;
        var arm      = this.arm1;
        var rotation = arm.rotation;

        if (!this.mouseRightOfArm) {
            offsetY *= -1;
        }

        var tip = new Phaser.Point(offsetX, offsetY);
        tip.rotate(0, 0, rotation);

        return {
            x: arm.world.x + tip.x,
            y: arm.world.y + tip.y,
        };

    },
    fire: function() {

        var tip = this.getGunTip();

        this.weapon.fireFrom.x = tip.x;
        this.weapon.fireFrom.y = tip.y;

        this.weapon.fireAtPointer();

        this.arm1.animations.play('fire');

    }
});

module.exports = Player;
},{}],9:[function(require,module,exports){
'use strict';

var Static = Phaser.Sprite.extend({
    constructor: function Sprite(game, x, y, texture) {
        texture = texture || game.cache.getBitmapData('box');

        Phaser.Sprite.call(this, game, x, y, texture);
        game.physics.enable(this, Phaser.Physics.ARCADE);

        this.body.allowGravity = false;
        this.body.immovable = true;
    },
    initProperties: function(props){
        props = props || {};
        if(
            props.alpha !== undefined &&
            props.alpha !== null
        ){
            this.alpha = parseFloat(props.alpha);
        }
    }
});

module.exports = Static;
},{}],10:[function(require,module,exports){
'use strict';



require('./lib/phaser-extend');

var states = {
    boot:      require('./states/0-boot'),
    preloader: require('./states/1-preloader'),
    init:      require('./states/2-init'),

    // 'level-0': require('./states/level-0'),
    'level-1': require('./states/level-1'),
};

var settings = {
    width:    800,
    height:   334,
    renderer: Phaser.WEBGL,
    // parent: 'game',
    transparent: false,
    antialias:   false,
    // state: null,
    // scaleMode: Phaser.ScaleManager.EXACT_FIT,
    enableDebug: true,
// resolution: null,
// preserveDrawingBuffer: null,
// physicsConfig: null,
// seed: null,
};

var game = window.game = new Phaser.Game(settings);

_.each(states, function(state, key) {
    game.state.add(key, state);
});

game.state.start('boot');


},{"./lib/phaser-extend":3,"./states/0-boot":11,"./states/1-preloader":12,"./states/2-init":13,"./states/level-1":14}],11:[function(require,module,exports){
'use strict';

var bootState = {

    init: function() {

    },
    create: function() {

        this.state.start('preloader', false);
    }
};


module.exports = bootState;

},{}],12:[function(require,module,exports){
'use strict';

var preloaderState = {

    preload: function() {


        this.load.spritesheet('player-arm', 'assets/player-arm.png', 32, 32);
        this.load.spritesheet('player', 'assets/player.png', 25, 46);

        var b = this.add.bitmapData(3, 3, 'bullet', true);
        b.ctx.fillStyle = 'black';
        b.ctx.fillRect(0, 0, 3, 3);

        var b = this.add.bitmapData(4, 4, 'box', true);
        b.ctx.fillStyle = 'black';
        b.ctx.fillRect(0, 0, 4, 4);

    },
    create: function() {

        this.state.start('init', false);
    },
};

module.exports = preloaderState;
},{}],13:[function(require,module,exports){
'use strict';


var initState = {
    create: function() {

        this.input.action = this.bindInputKeys();

        this.state.start('level-1', false);

    },

    bindInputKeys: function() {
        var k = Phaser.KeyCode;

        var keys = {
            jump:      k.SPACEBAR,
            jump_alt:  k.W,
            up:        k.UP,
            up_alt:    k.W,
            down:      k.DOWN,
            down_alt:  k.S,
            left:      k.LEFT,
            left_alt:  k.A,
            right:     k.RIGHT,
            right_alt: k.D,
        }
        var input = this.input;
        _.each(keys, function(binding, key) {
            keys[key] = input.keyboard.addKey(binding);
        });

        return keys;
    },
};


module.exports = initState;
},{}],14:[function(require,module,exports){
'use strict';

var Enemy           = require('../lib/sprite/enemy');
var Level           = require('../lib/level');
var LevelDataParser = require('../lib/level-data-parser.js');

var level1 = Level({
    init: function() {

        this.initCore();
    },
    preload: function() {
        this.load.tilemap('level-map', 'assets/level-1-map.json', null, Phaser.Tilemap.TILED_JSON);
        this.load.image('bg-city', 'assets/bg-city.png');
        this.load.image('collision-tiles', 'assets/collision-tiles.png');
    },
    create: function() {




        var level = this.bg = this.add.tilemap('level-map');
        level.addTilesetImage('collision-tiles');
        level.setCollisionByExclusion([]);
        this.collisionLayer = level.createLayer('collision-layer');

        var levelData    = this.cache.getTilemapData('level-map');
        var game         = this.game;

        var parser = LevelDataParser(game);
        parser.parse(levelData);

        var playerStartX = 250;
        var playerStartY = 60;

        this.player.x = playerStartX;
        this.player.y = playerStartY;

        var enemy = this.enemy = new Enemy(this.game, 300, 60);
        this.groups.enemies.add(enemy);
        this.stage.backgroundColor = '#808080';

    },

    update: function() {

        var groups = this.groups;
        var player = this.player;

        var bgLayer = this.collisionLayer;

        this.physics.arcade.collide(bgLayer, player);
        this.physics.arcade.collide(bgLayer, groups.enemies);
        this.physics.arcade.collide(bgLayer, groups.bullets, function(bullet, tile) {
            bullet.kill();
        });

        this.physics.arcade.collide(player, groups.enemies);

        this.physics.arcade.overlap(player, groups.level, function(player, target) {
            console.log('overlap: player, level');
                // console.log('player', player);
                // console.log('target', target);
        });

        this.physics.arcade.overlap(groups.bullets, groups.enemies, function(bullet, enemy){
            console.log('overlap: bullet, enemy');

            bullet.kill();
        });

        // does not work
        this.physics.arcade.overlap(groups.enemies, groups.level, function(bullet, tile) {
            console.log('enemy group collide with level group');
        });



        this.game.debug.body(player);
        this.game.debug.body(groups.level.children[0]);
        // this.game.debug.spriteBounds(player);
        // this.game.debug.spriteBounds(player.arm);
    },


    shutdown: function() {
        this.resetCore();
        this.bg.destroy();
        this.collisionLayer.destroy();
    },
});

module.exports = level1;
},{"../lib/level":2,"../lib/level-data-parser.js":1,"../lib/sprite/enemy":6}]},{},[10])
//# sourceMappingURL=build.js.map
