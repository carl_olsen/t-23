'use strict';

var stat = require('node-static');
var http = require('http');

var port = 8080;

var file = new stat.Server('./public');

http.createServer(function (request, response) {
    request.addListener('end', function () {
        //
        // Serve files!
        //
        file.serve(request, response);
    }).resume();
}).listen(port);

console.log('http://localhost:' + port);
