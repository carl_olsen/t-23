'use strict';

var Bullet    = require('./sprite/bullet');
var ClickArea = require('./sprite/click-area');
var Player    = require('./sprite/player');

var Level = function(obj) {

    var base = {
        initCore: function() {

            var game = this.game;

            this.physics.arcade.gravity.y = 1000;

            if (!game.player) {
                this.initGameObjects();
            }

            this.player  = game.player;
            this.sprites = game.sprites;
            this.groups  = game.groups;
        },
        initGameObjects: function() {
            var game = this.game;

            var bg            = this.add.group(this.world, 'bg');
            var fireClickArea = this.add.existing(new ClickArea(game))
            var level         = this.add.group(this.world, 'level');
            var enemies       = this.add.group(this.world, 'enemies');
            var player        = this.add.existing(new Player(game))

            fireClickArea.events.onInputDown.add(function(pointer) {
                player.fire();
            });

            game.player = player;

            game.sprites = {
                fireClickArea: fireClickArea,
            };

            game.groups = {
                bg:      bg,
                level:   level,
                bullets: player.weapon.bullets,
                enemies: enemies,
            };
        },

        initLevelData: function() {},

        resetCore: function() {
            this.groups.enemies.removeAll();
        },

    }

    return Object.assign(base, obj);

};

module.exports = Level;