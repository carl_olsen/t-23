'use strict';

var extend = function(parentConsturctor, subProto) {
    var c = subProto.constructor;
    c.prototype.constructor = c;
    c.prototype             = Object.assign(
        Object.create(parentConsturctor.prototype),
        subProto
    );
    return c;
};

Phaser.extend = extend;

var targets = [
    Phaser.Sprite,
    Phaser.Group
];

targets.forEach(function(target){
    target.extend = function(proto){
        return extend(target, proto);
    };
});
