'use strict';

var Player = Phaser.Sprite.extend({
    constructor: function Sprite(game, x, y, texture) {
        texture = texture || 'player';
        Phaser.Sprite.call(this, game, x, y, texture);

        game.physics.enable(this, Phaser.Physics.ARCADE);
        this.body.collideWorldBounds = true;
        this.body.setSize(
            14,
            40,
            0,
            0
        );
        this.anchor.set(0.5);
        this.lastJump           = 0;
        this.body.maxVelocity.y = 500;

        this.animations.add('idle', [1], 15, true);

        var legs = this.legs = game.add.sprite(0, 0, 'player');
        legs.anchor.set(0.5);
        legs.animations.add('idle', [0], 15, true);
        legs.animations.add('walk', [2, 3, 4, 5, 6, 7, 8, 9, 10, 11], 15, true);
        legs.animations.add('walk_reverse', [2, 3, 4, 5, 6, 7, 8, 9, 10, 11].reverse(), 15, true);

        var torso = this.torso = game.add.sprite(0, 0, 'player');
        torso.defaultY  = torso.y;
        torso.stepDownY = torso.y + 1;
        torso.anchor.set(0.5);
        torso.animations.add('idle', [12], 15, true);
        torso.animations.play('idle');

        var armX = -1;
        var armY = -10;

        var arm1 = this.arm1 = game.add.sprite(armX, armY, 'player-arm');
        arm1.defaultY  = arm1.y;
        arm1.stepDownY = arm1.y + 1;
        arm1.anchor.set(0.1, 0.5);
        arm1.animations.add('idle', [0], 15, true);
        arm1.animations.add('fire', [1, 2, 3, 2, 1, 0], 50);

        var arm2 = this.arm2 = game.add.sprite(armX, armY, 'player-arm');
        arm2.defaultY  = arm2.y;
        arm2.stepDownY = arm2.y + 1;
        arm2.anchor.set(0.1, 0.5);
        arm2.animations.add('idle', [8], 15, true);


        this.addChild(legs);

        this.addChild(arm2);
        this.addChild(torso);
        this.addChild(arm1);

        this.animations.play('idle');
        legs.animations.play('idle');
        arm1.animations.play('idle');
        arm2.animations.play('idle');


        var weapon = game.add.weapon(20, game.cache.getBitmapData('bullet'));

        weapon.bulletKillType = Phaser.Weapon.KILL_WORLD_BOUNDS;
        weapon.bulletSpeed    = 800;
        weapon.fireRate       = 60;
        weapon.bullets.setAll('body.allowGravity', false);

        this.weapon = weapon;

    },
    update: function() {
        var game      = this.game;
        var keyAction = game.input.action;
        var LEFT      = keyAction.left.isDown || keyAction.left_alt.isDown;
        var RIGHT     = keyAction.right.isDown || keyAction.right_alt.isDown;
        var JUMP      = keyAction.jump.isDown || keyAction.jump_alt.isDown || keyAction.up.isDown || keyAction.up_alt.isDown;

        var walkVel      = 100;
        var jumpVel      = 300;
        var jumpCooldown = 750;

        this.body.velocity.x = 0;

        var mx          = game.input.activePointer.worldX;
        var mRightOfArm = this.mouseRightOfArm = mx > this.arm1.world.x;

        if (
            JUMP &&
            (this.body.touching.down || this.body.onFloor()) &&
            game.time.now > this.lastJump
        ) {
            this.body.velocity.y = -jumpVel;
            this.lastJump        = game.time.now + jumpCooldown;
        }

        var legsAnim;

        this.legs.scale.x = mRightOfArm ? 1 : -1;
        if (LEFT) {
            this.body.velocity.x = -walkVel;
            legsAnim             = mRightOfArm ? 'walk_reverse' : 'walk';

        } else if (RIGHT) {
            this.body.velocity.x = walkVel;
            legsAnim             = mRightOfArm ? 'walk' : 'walk_reverse';

        } else {
            legsAnim = 'idle';
        }

        this.legs.animations.play(legsAnim);

        this.arm1.rotation = this.arm2.rotation = game.physics.arcade.angleToPointer(this.arm1.world);

        var currentLegFrame = this.legs.animations.currentFrame.index;
        var stepDownFrames  = [1, 2, 6, 7];

        if (stepDownFrames.indexOf(currentLegFrame) !== -1) {
            this.arm1.y  = this.arm1.stepDownY;
            this.arm2.y  = this.arm2.stepDownY;
            this.torso.y = 1;
        } else {
            this.arm1.y  = this.arm1.defaultY;
            this.arm2.y  = this.arm2.defaultY;
            this.torso.y = 0;
        }

        this.torso.scale.x = mRightOfArm ? 1 : -1;
        this.arm1.scale.y  = this.arm2.scale.y = mRightOfArm ? 1 : -1;
    },
    getGunTip: function() {
        var offsetX  = 24;
        var offsetY  = -2;
        var arm      = this.arm1;
        var rotation = arm.rotation;

        if (!this.mouseRightOfArm) {
            offsetY *= -1;
        }

        var tip = new Phaser.Point(offsetX, offsetY);
        tip.rotate(0, 0, rotation);

        return {
            x: arm.world.x + tip.x,
            y: arm.world.y + tip.y,
        };

    },
    fire: function() {

        var tip = this.getGunTip();

        this.weapon.fireFrom.x = tip.x;
        this.weapon.fireFrom.y = tip.y;

        this.weapon.fireAtPointer();

        this.arm1.animations.play('fire');

    }
});

module.exports = Player;