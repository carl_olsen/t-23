'use strict';

var Enemy = Phaser.Sprite.extend({
    constructor: function Sprite(game, x, y, texture) {
        texture = texture || game.cache.getBitmapData('box');

        Phaser.Sprite.call(this, game, x, y, texture);

        game.physics.enable(this, Phaser.Physics.ARCADE);

        this.anchor.set(0.5);
        this.width  = 20;
        this.height = 20;
    },
    update: function() {
        // this.body.velocity.x = 0;
    }
});

module.exports = Enemy;