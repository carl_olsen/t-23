'use strict';

var ClickArea = Phaser.Sprite.extend({
    constructor: function Sprite(game, x, y, texture) {
        texture = texture || game.cache.getBitmapData('box');

        Phaser.Sprite.call(this, game, x, y, texture);

        this.alpha        = 0;
        this.width        = game.width;
        this.height       = game.height;
        this.inputEnabled = true;
   }
});

module.exports = ClickArea