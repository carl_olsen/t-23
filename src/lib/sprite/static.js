'use strict';

var Static = Phaser.Sprite.extend({
    constructor: function Sprite(game, x, y, texture) {
        texture = texture || game.cache.getBitmapData('box');

        Phaser.Sprite.call(this, game, x, y, texture);
        game.physics.enable(this, Phaser.Physics.ARCADE);

        this.body.allowGravity = false;
        this.body.immovable = true;
    },
    initProperties: function(props){
        props = props || {};
        if(
            props.alpha !== undefined &&
            props.alpha !== null
        ){
            this.alpha = parseFloat(props.alpha);
        }
    }
});

module.exports = Static;