'use strict';

var Bullet = Phaser.Sprite.extend({
    constructor: function Sprite(game, x, y, texture) {
        texture = texture || game.cache.getBitmapData('bullet');

        Phaser.Sprite.call(this, game, x, y, texture);

        game.physics.enable(this, Phaser.Physics.ARCADE);

        this.anchor.set(0.5);
        this.checkWorldBounds = true;
        this.outOfBoundsKill  = true;

        this.body.allowGravity = false;
    }
});

module.exports = Bullet;