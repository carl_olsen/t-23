'use strict';

var spriteTypes = require('./sprite');


var LevelDataParser = {
    game: null,

    parse: function(levelData) {
        var self = this;
        levelData.data.layers.forEach(function(layer) {
            if (layer.type === 'imagelayer') {
                self.loadImageLayer(layer);
            }

            if (layer.type === 'objectgroup') {
                self.loadObjectGroupLayer(layer);
            }
        });
    },

    loadImageLayer: function(layer) {
        var sprite = this.game.add.sprite(layer.x, layer.y, layer.name);
        this.game.groups.bg.add(sprite);
    },

    loadObjectGroupLayer: function(layer) {
        var game = this.game;

        layer.objects.forEach(function(obj) {
            var properties = obj.properties || {};
            var type       = properties.type;
            var SpriteType = spriteTypes[type];
            if (!SpriteType) {
                return;
            }

            var sprite = new SpriteType(game, obj.x, obj.y);
            sprite.name   = obj.name;
            sprite.width  = obj.width;
            sprite.height = obj.height;

            if (sprite.initProperties) {
                sprite.initProperties(properties);
            }

            game.groups.level.add(sprite);
        });
    }
};


var factory = function(game) {
    var obj = Object.create(LevelDataParser);
    obj.game = game;
    return obj;
};
module.exports = factory;