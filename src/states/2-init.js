'use strict';


var initState = {
    create: function() {

        this.input.action = this.bindInputKeys();

        this.state.start('level-1', false);

    },

    bindInputKeys: function() {
        var k = Phaser.KeyCode;

        var keys = {
            jump:      k.SPACEBAR,
            jump_alt:  k.W,
            up:        k.UP,
            up_alt:    k.W,
            down:      k.DOWN,
            down_alt:  k.S,
            left:      k.LEFT,
            left_alt:  k.A,
            right:     k.RIGHT,
            right_alt: k.D,
        }
        var input = this.input;
        _.each(keys, function(binding, key) {
            keys[key] = input.keyboard.addKey(binding);
        });

        return keys;
    },
};


module.exports = initState;