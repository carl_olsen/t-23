'use strict';

var preloaderState = {

    preload: function() {


        this.load.spritesheet('player-arm', 'assets/player-arm.png', 32, 32);
        this.load.spritesheet('player', 'assets/player.png', 25, 46);

        var b = this.add.bitmapData(3, 3, 'bullet', true);
        b.ctx.fillStyle = 'black';
        b.ctx.fillRect(0, 0, 3, 3);

        var b = this.add.bitmapData(4, 4, 'box', true);
        b.ctx.fillStyle = 'black';
        b.ctx.fillRect(0, 0, 4, 4);

    },
    create: function() {

        this.state.start('init', false);
    },
};

module.exports = preloaderState;