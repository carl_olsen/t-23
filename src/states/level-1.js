'use strict';

var Enemy           = require('../lib/sprite/enemy');
var Level           = require('../lib/level');
var LevelDataParser = require('../lib/level-data-parser.js');

var level1 = Level({
    init: function() {

        this.initCore();
    },
    preload: function() {
        this.load.tilemap('level-map', 'assets/level-1-map.json', null, Phaser.Tilemap.TILED_JSON);
        this.load.image('bg-city', 'assets/bg-city.png');
        this.load.image('collision-tiles', 'assets/collision-tiles.png');
    },
    create: function() {




        var level = this.bg = this.add.tilemap('level-map');
        level.addTilesetImage('collision-tiles');
        level.setCollisionByExclusion([]);
        this.collisionLayer = level.createLayer('collision-layer');

        var levelData    = this.cache.getTilemapData('level-map');
        var game         = this.game;

        var parser = LevelDataParser(game);
        parser.parse(levelData);

        var playerStartX = 250;
        var playerStartY = 60;

        this.player.x = playerStartX;
        this.player.y = playerStartY;

        var enemy = this.enemy = new Enemy(this.game, 300, 60);
        this.groups.enemies.add(enemy);
        this.stage.backgroundColor = '#808080';

    },

    update: function() {

        var groups = this.groups;
        var player = this.player;

        var bgLayer = this.collisionLayer;

        this.physics.arcade.collide(bgLayer, player);
        this.physics.arcade.collide(bgLayer, groups.enemies);
        this.physics.arcade.collide(bgLayer, groups.bullets, function(bullet, tile) {
            bullet.kill();
        });

        this.physics.arcade.collide(player, groups.enemies);

        this.physics.arcade.overlap(player, groups.level, function(player, target) {
            console.log('overlap: player, level');
                // console.log('player', player);
                // console.log('target', target);
        });

        this.physics.arcade.overlap(groups.bullets, groups.enemies, function(bullet, enemy){
            console.log('overlap: bullet, enemy');

            bullet.kill();
        });

        this.physics.arcade.overlap(groups.enemies, groups.level, function(bullet, tile) {
            console.log('enemy group collide with level group');
        });

        this.game.debug.body(player);
        this.game.debug.body(groups.level.children[0]);
        // this.game.debug.spriteBounds(player);
        // this.game.debug.spriteBounds(player.arm);
    },


    shutdown: function() {
        this.resetCore();
        this.bg.destroy();
        this.collisionLayer.destroy();
    },
});

module.exports = level1;