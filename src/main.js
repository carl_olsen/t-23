'use strict';



require('./lib/phaser-extend');

var states = {
    boot:      require('./states/0-boot'),
    preloader: require('./states/1-preloader'),
    init:      require('./states/2-init'),

    // 'level-0': require('./states/level-0'),
    'level-1': require('./states/level-1'),
};

var settings = {
    width:    800,
    height:   334,
    renderer: Phaser.WEBGL,
    // parent: 'game',
    transparent: false,
    antialias:   false,
    // state: null,
    // scaleMode: Phaser.ScaleManager.EXACT_FIT,
    enableDebug: true,
// resolution: null,
// preserveDrawingBuffer: null,
// physicsConfig: null,
// seed: null,
};

var game = window.game = new Phaser.Game(settings);

_.each(states, function(state, key) {
    game.state.add(key, state);
});

game.state.start('boot');

